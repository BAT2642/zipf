"""One-line description of what the script does."""

import sys

def main(name):
    """Run the program."""
    print(f'Hello {name}')

if __name__ == '__main__':
    args = sys.argv[1:]
    main(*args)